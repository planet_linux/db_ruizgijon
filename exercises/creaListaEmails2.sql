USE classicmodels;

DELIMITER //
CREATE PROCEDURE creaListaEmails2 (OUT listaEmails TEXT)
BEGIN
	DECLARE lista_temporal TEXT  DEFAULT "";
	DECLARE fin INTEGER DEFAULT 0;
	DECLARE un_email varchar(400) DEFAULT "";
	DECLARE contador INTEGER DEFAULT 0;
	DECLARE cursorEmail CURSOR FOR 
			SELECT email FROM employees;

	-- declara un manejador del tipo NOT FOUND
	DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET fin = 1;
	
	OPEN cursorEmail;

	getEmail: LOOP
		FETCH cursorEmail INTO un_email;
		IF fin = 1 THEN 
			LEAVE getEmail;
		END IF;
		
		IF contador = 0 THEN
			SET lista_temporal = un_email;
		ELSE
			SET lista_temporal = CONCAT(lista_temporal,";",un_email);
		END IF;	
		
		SET contador = contador + 1;
		
	END LOOP getEmail;
	CLOSE cursorEmail;
	SET listaEmails = lista_temporal;
END //
DELIMITER ;


-- llamando al procedimiento
USE classicmodels;
SET @lista = ""; 
CALL creaListaEmails2(@lista); 
SELECT @lista;
